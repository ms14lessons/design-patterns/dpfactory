package example1;

public class PhoneFactory {
    public static Phone getPhone(String model, String battery, int wide, int height) {

        Phone phone;
        if ("SamsungS8".equalsIgnoreCase(model)) {
            phone = new SamsungS8(model, battery, wide, height);
        } else if ("SamsungNote8".equalsIgnoreCase(model)) {
            phone = new SamsungNote8(model, battery, wide, height);
        } else {
            throw new RuntimeException("Not valid model");
        }
        return phone;
    }


}
