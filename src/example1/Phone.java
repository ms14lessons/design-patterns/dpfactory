package example1;

public interface Phone {
    String getModel();

    String getBattery();

    int getWide();

    int getHeight();

}
